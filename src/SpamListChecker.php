<?php

namespace idfortysix\spamlistchecker;

use Exception;
use DNSBL\DNSBL;

class SpamListChecker
{
    private $interfaces;
    private $checkedRecords = [];
    private $blacklistedIn = [];
    private $dnsErrors = [];
    
    public function __construct()
    {
        $this->interfaces = function_exists('config') 
            ? config('services.dispatch.interfaces') 
            : json_decode($_ENV['INTERFACES'], true);
    }

    public function execute()
    {
        $records = $this->setRecords();

        foreach($records as $record)
        {
            $this->lookup($record);

            $this->checkedRecords[] = $record;
        }

        if($this->blacklistedIn)
        {
            $warning = 'WARNING - ';

            foreach($this->blacklistedIn as $key => $item)
            {
                $warning .= $key . ' : ' . implode(' ', $item);
                $warning .= "\n";
            }

            foreach($this->dnsErrors as $err_item)
            {
                $warning .= $err_item;
                $warning .= "\n";
            }

            echo $warning;
        }
        else
        {
            echo "OK - records: ". implode(' ', $this->checkedRecords) . "\n";
        }
    }

    private function lookup(string $record) 
    {
        foreach($this->blacklists() as $blacklist)
        {
            $dnsbl = new DNSBL([
                'blacklists' => [$blacklist]
            ]);

            try {
                if($dnsbl->isListed($record)) 
                {
                    $this->blacklistedIn[$record][] = $blacklist;
                }
            } catch(Exception $e) {
                $this->dnsErrors[] = "$record $blacklist dns check failed.";    
            }
        }

        return;
    }

    private function setRecords()
    {
        $records = [];

        foreach($this->interfaces as $interface)
        {
            if(isset($interface['host']))
            {
                $records[] = $interface['host'];

                // atskirai checkinam spamhaus dnbl domenams
                $this->lookupDblSpamhaus($interface['host']);
            }

            if(isset($interface['ip']))
            {
                $records[] = $interface['ip'];
            }
        }

        return $records;
    }

    private function blacklists() 
    {
        return [
            'b.barracudacentral.org',
            'bl.blocklist.de',
            'bl.mailspike.net',
            'bl.score.senderscore.com',
            'bl.spamcop.net',
            'cbl.abuseat.org',
            'dnsbl.cobion.com',	
            'dnsbl.sorbs.net',
            'dyna.spamrats.com',
            'pbl.spamhaus.org',
            'psbl.surriel.com',
            'spam.dnsbl.sorbs.net',	
            'sbl.spamhaus.org',
            'truncate.gbudb.net',
            'xbl.spamhaus.org',
            'zen.spamhaus.org'
        ];
    }

    private function lookupDblSpamhaus($domain_name)
    {  
        try
        {  
            if(checkdnsrr($domain_name . '.dbl.spamhaus.org', "A"))
            {
                $this->blacklistedIn[$domain_name][] = 'dbl.spamhaus.org';
            }
        }
        catch(Exception $e) 
        {
            $this->dnsErrors[] = "$domain_name dbl.spamhaus.org dns check failed.";
        }
       
        return;
    }
}
