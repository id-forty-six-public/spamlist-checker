## Install
```
./composer.phar require id-forty-six/spamlist-checker
```

## Usage
Takes IP and host from ENV INTERFACES and makes DNS lookup on blacklists
```
(new SpamListChecker)->execute()
```